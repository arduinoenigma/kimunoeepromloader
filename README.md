# KIMUnoEEPROMLoader

A sketch to view and load EEPROM contents for the KIM Uno without having to hand enter them.

it has some experiments, as well as a Clock program and an Enigma Z30 Machine Simulator

0x400 - CLOCK [AD]0024 SEC [AD]0025 MIN [AD]0026 HOUR
0x450 - DISPLAY KEYS USING 1F6A ROUTINE
0x460 - DEADBEEF AND DISPLAY KEYS WITH 1F1F ROUTINE
0x490 - EMPTY
0x500 - ENIGMA Z
0x7B0 - EMPTY

Kim Uno:
https://obsolescence.wixsite.com/obsolescence/kim-uno-summary-c1uuh

Enigma Z30 Simulator:
https://arduinoenigma.blogspot.com/p/enigma-z30-for-kim-uno.html

Clock:
https://hackaday.com/2015/09/29/kim-1-clock/